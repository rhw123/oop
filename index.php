<?php 

require_once "animal.php";
require_once "frog.php";
require_once "ape.php";

$panggil = new Animal("shaun");

echo "name : " . $panggil->name . "<br>";
echo "legs : " . $panggil->legs. "<br>";
echo  "cold_blooded : " .  $panggil->cold_blooded . "<br><br>";

$panggil2 = new Frog("buduk");
echo  "name : " . $panggil2->name . "<br>";
echo "legs  : " . $panggil2->legs. "<br>";
echo "cold_blooded : " . $panggil2->cold_blooded. "<br>";
echo $panggil2->frog(). "<br><br>";

$panggil3 = new App("kera sakti");
echo "name : " . $panggil3->name . "<br>";
echo "legs : " . $panggil3->legs. "<br>";
echo "cold_blooded : " . $panggil3->cold_blooded . "<br>";
echo $panggil3->yell();

?>